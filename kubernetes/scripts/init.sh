#!/bin/bash

gcloud auth activate-service-account --key-file=service_account.json
gcloud container clusters get-credentials $GCP_KE_CLUSTER --zone $GCP_ZONE --project $GCP_PROJECT
#gcloud config set project $GCP_PROJECT


echo "---Deploy Tiller---"
kubectl apply -f manifests/tiller/tiller-sa.yml
helm init --service-account tiller
#helm init --upgrade
kubectl rollout status -n "$TILLER_NAMESPACE" -w "deployment/tiller-deploy"


echo "---Deploy NGINX ingress---"
kubectl describe namespace $NGINX_NAMESPACE || kubectl create namespace $NGINX_NAMESPACE
helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true --namespace $NGINX_NAMESPACE
sleep 

ext_ip=$"{kubectl get svc -n nginx-ingress nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}'}"
echo "---Add DNS Record---"    
gcloud dns record-sets transaction start -z=$GCP_DNS_ZONE_NAME  
gcloud dns record-sets transaction add -z=$GCP_DNS_ZONE_NAME --type=A --name="production.tennki.tk." --ttl 600 "$ext_ip"
gcloud dns record-sets transaction execute -z=$GCP_DNS_ZONE_NAME


echo "---Deploy Cert-Manager---"
kubectl describe namespace $CERTMANAGER_NAMESPACE || kubectl create namespace $CERTMANAGER_NAMESPACE
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml
kubectl label namespace $CERTMANAGER_NAMESPACE certmanager.k8s.io/disable-validation=true
kubectl apply -f manifests/cert-manager/cluster-issuers.yaml
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install --name cert-manager --namespace $CERTMANAGER_NAMESPACE jetstack/cert-manager --set ingressShim.defaultIssuerName=letsencrypt-prod --set ingressShim.defaultIssuerKind=ClusterIssuer
