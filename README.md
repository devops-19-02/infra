# Проектная работа по курсу ОТУС: "DevOps - практики и инструменты"

# Инфраструктура

## План работ:
- [Х] Создать репозиторий в GitLab
- [X] Развернуть инфраструктуру в GCP
    - [X] Развернутьт кластер GKE используя подход IasC
        - [X] Установить NGINX Ingress
        - [X] Установить Helm
        - [X] Настроить cert-manager
        - [X] Автоматизация
    - [X] Создать сервисный аккаунт для доступа к ресурсам GCP
    - [X] Развернуть инфраструктуру для мониторинга
    - [X] Настроить Cloud DNS
    - [X] Сделать регистрацию имен в DNS
- [ ] Настроить логирование (опционально)
- [ ] Настроить трейсинг (опционально)
- [ ] Настроить ChatOps (опционально)
    - [X] Настроить оповещение в Slack


## Приложение
Приложение находится в отдельном репозитории https://gitlab.com/devops-19-02/project-a

## Инфраструктура
Инфраструктура представляет GKE кластер. 

![GKE cluster](img/gke.jpg "GKE cluster")

Развернтывание выполянется из каталога terraform.

```bash
terraform init
terraform apply
```

При создании GKE кластера удаляется default node pool и создается кастомный, с заданным именем и конфигурацией нод. 
При развертывании в кластере запускается tiller, nginx-ingress и cert-manager, также регистрируется dns запись для внешнего ip ingress. Все остальные ресурсы публикуются в Cloud DNS как CNAME (при желаии можно переделать на А-записи). Создание и запуск дополнительных компонентов выполяется скриптом kubernetes/scripts/init.sh, который запускается как local-exex provisioner из main.tf
- Сert-manager предназначен для автоматического запроса сертификатов с Let's encrypt для публикуемых ресурсов. Используются 2 ClusterIssuer: letsencrypt-prod для запроса "боевых" сертификатов, letsencrypt-staging для выпуска "тестовых" сертификатов. Данные ClusterIssuer описаны в файле kubernetes/manifests/cert-manager/cluster-issuers.yaml
- Для запуска tiller используется сервисный аккаунт, который описан в файле kubernetes/manifests/tiller/tiller-sa.yml

Компоненты запускаются в трех нейспейсах:

Компонент | Неймспейс
--------- | ----------
`nginx-ingress` | `nginx-ingress`
`cert-manager` | `cert-manager`
`tiller` | `kube-system`

В файле terraform/terraform.tfvars.example приведен пример данных необходимых для развертывания.
Описание переменных:

Параметр | Описание | Значение по умолчанию
--------- | ----------- | -------
`project` | GCP ID проекта | ``
`region` | GCP регион | `europe-west3`
`zone` | GCP зона | `europe-west3-a`
`cluster_name` | Имя кластера GKE | ``
`node_count` | Количество нод в кластере GKE | `1`
`tiller_namespace` | Неймспейс для запуска tiller-а | ``
`nginx_namespace` | Неймспейс для запуска nginx-ingress | ``
`certmanager_namespace` | Неймспейс для запуска cert-manager | ``
`dns_zone_name` | Имя зоны в Cloud DNS | ``


## Мониторинг
Мониторинг представлен стеком prometheus+grafana. Для получения метрик от MongoDB использован prometheus-mongodb-exporter. Все компоненты собраны в один helm чарт (kubernetes/charts/monitor). Grafana запускается преднастроенная, с дашбордами (kubernetes/charts/grafana/dashboards, при необходимости добавить новые дашборды необходимо подгрузить json-ы в данный каталог) и источником данных (по умолчанию выбирается prometheus сервер из того же неймспейса и helm релиза). 

Поиск endpoint-ов сделан через service discovery. Описание джобов приведены в файле kubernetes/charts/prometheus/values.yaml
```yaml
...
- job_name: 'se-production'
        kubernetes_sd_configs:
          - role: endpoints
        relabel_configs:
          - action: labelmap
            regex: __meta_kubernetes_service_label_(.+)
          - source_labels: [__meta_kubernetes_service_label_app, __meta_kubernetes_namespace]
            action: keep
            regex: se;production
          - source_labels: [__meta_kubernetes_namespace]
            target_label: kubernetes_namespace
          - source_labels: [__meta_kubernetes_service_name]
            target_label: kubernetes_name
...
```

## CI/CD
При внесении изменений в компоненты мониторинга происходит деплой на стейджинг окружение для ревью и тестирования. После тестирования нужно удалить стейдж окружение (для экономии ресурсов). Деплой в продакшен окружение выполняется из ветки мастер при ручном запуске задания.
Ресурсы мониторинга запускаются в неймспейсе monitoring. Тестирование происходит в неймспейсе monitoring-dev.

Переменные | Описание
--------- | -----------
`GCP_KE_CLUSTER` | `Имя GKE кластера`
`GCP_PROJECT` | `ID GCP проекта`
`GCP_ZONE` | `GCP зона`
`GITLAB_SA_KEY` | `json сервис-аккаунта для доступа gitlab к GKE кластеру`
`GRAFANA_PROD_PWD` | `Продакшен пароль администратора Grafana`
`GRAFANA_STAGE_PWD` | `Стейджинг пароль администратора Grafana`
`MONGODB_URI` | `URI MongoDB инстансов в формате [mongodb://][user:pass@]host1[:port1][,host2[:port2],...][/database][?options]`
`PROD_CERT_ISSUER` | `Cert-manager ClusterIssuer для запроса боевых сертификатов`
`STAGE_CERT_ISSUER` | `Cert-manager ClusterIssuer для запроса тестовых сертификатов`

При деплое переменные HOST и URL в файле kubernetes/charts/monitor/values.yaml заменяются на реальные имена. 
```yaml
grafana:
  ingress:
    tls: 
    - hosts:
      - HOST
      secretName: HOST
  datasources:
    datasources.yaml:
      apiVersion: 1
      datasources:
      - name: Prometheus
        type: prometheus
        url: URL
        access: proxy
        isDefault: true

```

## Проверка работы
Grafana доступна по адресу https://grafana.tennki.tk
