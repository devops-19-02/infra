terraform {
  required_version = ">=0.11,<0.12"
}


provider "google" {
  version = "2.0.0"
  project = "${var.project}"
  region  = "${var.region}"
}

resource "google_container_cluster" "primary" {
  name     = "${var.cluster_name}"
  region   = "${var.zone}"

  remove_default_node_pool = true
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  addons_config {
    kubernetes_dashboard {
      disabled = false
    }
  }
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.cluster_name}-pool"
  region     = "${var.zone}"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = "${var.node_count}"
  node_config {
    machine_type = "g1-small"

    disk_size_gb = "10"
    
    metadata {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  provisioner "local-exec" {
    command = "/bin/bash scripts/init.sh"
    working_dir = "../kubernetes/"
    environment = {
      GCP_KE_CLUSTER = "${var.cluster_name}"
      GCP_ZONE = "${var.zone}"
      GCP_PROJECT = "${var.project}"
      GCP_DNS_ZONE_NAME = "${var.dns_zone_name}"
      TILLER_NAMESPACE = "${var.tiller_namespace}"
      NGINX_NAMESPACE = "${var.nginx_namespace}"
      CERTMANAGER_NAMESPACE = "${var.certmanager_namespace}"
    }
  }
}
