variable project {
  description = "Project ID"
}

variable region {
  description = "Region"
  default     = "europe-west3"
}

variable zone {
  description = "Zone"
  default     = "europe-west3-a"
}

variable cluster_name {
  description = "GKE cluster name"
}

variable node_count {
  description = "GKE pool node count"
  default     = 1
}

variable dns_zone_name {
  description = "GCP cloud DNS zone name"
}

variable tiller_namespace {
  description = "Tiller namespace"
}

variable nginx_namespace {
  description = "NGINX ingress namespace"
}

variable certmanager_namespace {
  description = "Cert-manager namespace"
}
